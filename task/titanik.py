import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    mr_df = df.loc[("Mr." in df["Name"]), ["Age"]]
    mrs_df = df.loc[("Mrs." in df["Name"]), ["Age"]]
    miss_df = df.loc[("Miss." in df["Name"]), ["Age"]]

    a = ('Mr.', mr_df.isna().sum(), mr_df.median(skipna=True))
    b = ('Mrs.', mrs_df.isna().sum(), mrs_df.median(skipna=True))
    c = ('Miss.', miss_df.isna().sum(), miss_df.median(skipna=True))

    return [a, b, c]
